from flask import Flask

## Flask app factory pattern
## call this function whenerver you want to create an instance of flask app
def create_app():
    """
    Create a Flask application using the app factory pattern.

    :return: Flask app
    """
    app = Flask(__name__, instance_relative_config=True)

    # config settings would be used in development for non-prod env, password,etc
    # look for settings in config folder
    app.config.from_object('config.settings')
    # instance settings file would contain private settings like API Keys, passwords,etc
    # not put in version control
    # looks for settings in instance folder
    app.config.from_pyfile('settings.py', silent=True)

    # using decorator to supply url end point
    @app.route('/')
    def index():
        #example of multi-line comments
        """
        Render a Hello World response.

        :return: Flask response
        """
        # retrieving text from settings.py in  config directory
        return app.config['HELLO']

    return app
